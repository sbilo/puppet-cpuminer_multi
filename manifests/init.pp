# Class: cpuminer_multi
# ===========================
#
# Full description of class claymore here.
#
# Parameters
# ----------
#
# Authors
# -------
#
# Author Name <sander.bilo@gmail.com>
#
# Copyright
# ---------
#
# Copyright 2017 Sander Bilo, unless otherwise noted.
#
class cpuminer_multi(
  $enabled   = false,
  $algorithm = 'cryptonight',
  $pool      = 'stratum+tcp://mine.moneropool.com:3333',
  $wallet    = '45Fc2LVYoJwfo5wUJ2KZfnA6KfA657qf5YZoLeEKiJBy8xqCMibZunGS9TfNwTXttk2X9xyvLfUYLHPYYHt27UY5SHBzCi7',
  $password  = 'x'
) {

  contain cpuminer_multi::install
  contain cpuminer_multi::config
  contain cpuminer_multi::service

  Class['::cpuminer_multi::install']
  -> Class['::cpuminer_multi::config']
  ~> Class['::cpuminer_multi::service']
}
