class cpuminer_multi::service inherits cpuminer_multi {

  file { '/lib/systemd/system/cpuminer.service':
    ensure  => file,
    content => template('cpuminer_multi/cpuminer.service.erb'),
    notify  => Exec['cpuminer:daemon-reload']
  }

  exec {'cpuminer:daemon-reload':
    command     => '/bin/systemctl daemon-reload',
    refreshonly => true,
    notify      => Service['cpuminer']
  }

  service { 'cpuminer':
    ensure => $cpuminer_multi::enabled ? {
      true  => running,
      false => stopped,
    },
    enable => $cpuminer_multi::enabled
  }

}