class cpuminer_multi::install inherits cpuminer_multi {

  ensure_packages(['libjansson4','libcurl3'], {'ensure' => 'present'})

  file { '/usr/bin/cpuminer':
    ensure => present,
    source => 'puppet:///modules/cpuminer_multi/cpuminer',
    owner  => 'root',
    group  => 'root',
    mode   => '0744'
  }
}